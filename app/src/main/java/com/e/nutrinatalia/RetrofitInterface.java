package com.e.nutrinatalia;

import java.util.Map;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface RetrofitInterface {
    @Headers({
            "Accept: application/json",
            "Content-Type: application/json"
    })
    @POST("/stock-nutrinatalia/persona")
//    Call<User> uploadReg(@Body User user);
    Call<Object> uploadReg(@Body Map<String, String> body);

    @GET("/stock-nutrinatalia/persona")
    Call<ResponseBody> getData();

}