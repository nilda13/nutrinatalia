package com.e.nutrinatalia;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class DashboarWindow extends AppCompatActivity {
    TextView welcome_user;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboar_window);
        welcome_user = findViewById(R.id.welcome_user);

        Intent intent = getIntent();
        if(intent.getExtras() != null){
            String passedUsername = intent.getStringExtra("nombre");
            String welcome = "Bienvenido" +" "+ passedUsername;
            welcome_user.setText(welcome);

        }
    }
}
