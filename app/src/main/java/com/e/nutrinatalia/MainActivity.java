package com.e.nutrinatalia;

import androidx.appcompat.app.AppCompatActivity;
import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = registrar_person.class.getSimpleName();
    boolean flag = false;

    Button log_button;
    Button reg_button;
    EditText txt_user;
    EditText txt_pass;

    public static final String URL = conection.getPuerto();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        log_button = findViewById(R.id.log_button);
        reg_button = findViewById(R.id.reg_button);
        txt_user = findViewById(R.id.txt_user);
        txt_pass = findViewById(R.id.txt_pass);

        log_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(MainActivity.this,"Logueado",Toast.LENGTH_SHORT).show();
//                Intent i =new Intent(MainActivity.this, DashboarWindow.class);
//                startActivity(i);
                login();
            }
        });

        reg_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i =new Intent(MainActivity.this, registrar_person.class);
                startActivity(i);

            }
        });
    }

    private void login() {
        Toast.makeText(MainActivity.this, "Data Login", Toast.LENGTH_SHORT).show();


        OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                .connectTimeout(90, TimeUnit.SECONDS)
                .readTimeout(90, TimeUnit.SECONDS)
                .writeTimeout(90, TimeUnit.SECONDS)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        RetrofitInterface retrofitInterface = retrofit.create(RetrofitInterface.class);

        final String nombre = txt_user.getText().toString();

        Call<ResponseBody> call = retrofitInterface.getData();
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {

                    try {
                        JSONObject jsonlogin = new JSONObject(response.body().string());
                        JSONArray myResponse = jsonlogin.getJSONArray("lista");
                        ArrayList<String> listdata = new ArrayList<String>();
                        for (int i = 0; i < myResponse.length(); i++) {
                            String nomb = myResponse.getJSONObject(i).getString("nombre");
                            String ape = myResponse.getJSONObject(i).getString("apellido");

                            // String ape = myResponse.getJSONObject(i).getString("apelli");

                            // JSONObject obj = myResponse.getJSONObject(i);
                            //
                            // if (obj.getString("nombre").equals(nombre)){
                            // flag = true;
                            //  oast.makeText(MainActivity.this,"El Usuario se encuentra", Toast.LENGTH_SHORT).show();
                            //  break;
                            //  }else{
                            //  Toast.makeText(MainActivity.this,"El Usuario no se encuentra", Toast.LENGTH_SHORT).show();
                            //  }
                            listdata.add(nomb);
                        }
                        if(listdata.contains(nombre)){
                            Toast.makeText(MainActivity.this,"El Usuario registrado y Redireccionado!", Toast.LENGTH_SHORT).show();
                            Intent dash =new Intent(MainActivity.this, DashboarWindow.class);
                            dash.putExtra("nombre",nombre);
                            //startActivity(dash);
                            startActivityForResult(dash,2000);
                        }else{
                            Toast.makeText(MainActivity.this,"El Usuario no se encuentra registrado en la BD!...UWU", Toast.LENGTH_SHORT).show();
                            Intent reg_per =new Intent(MainActivity.this, registrar_person.class);
                            startActivity(reg_per);
                        }


                        //  Log.d("Lista", String.valueOf(myResponse));
//                        for(int i=0; i< myResponse.length(); i++){
//                            JSONObject obj = myResponse.getJSONObject(i);
//                            //  Log.d(TAG, "DATOS:"+obj.getString("nombre"));
//                            if (obj.getString("nombre").equals(txt_user)){
//                                Toast.makeText(MainActivity.this,"Usuario Existente y Logueado!",Toast.LENGTH_SHORT).show();
//                                break;
//                            }else{
//                                Toast.makeText(MainActivity.this,"El usuario no se encuentra registrado en la BD!",Toast.LENGTH_SHORT).show();
//                            }
//
//
//                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(MainActivity.this,"Error al Obtener Resultados",Toast.LENGTH_SHORT).show();
            }
        });
    }
}
