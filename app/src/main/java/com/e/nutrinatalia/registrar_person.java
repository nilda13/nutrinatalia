package com.e.nutrinatalia;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;

import org.json.JSONException;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import org.json.JSONException;
import org.json.JSONObject;

public class registrar_person extends AppCompatActivity {

    public static final String TAG = registrar_person.class.getSimpleName();

    EditText txt_nombre;
    EditText txt_apellido;
    EditText txt_correo;
    EditText txt_tel;
    EditText txt_ruc;
    EditText txt_ci;
    EditText txt_person;
    EditText txt_fecha_nacimiento;
    Button  reg_button;

    public static final String URL = conection.getPuerto();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrar_person);
        txt_nombre = findViewById(R.id.txt_nombre);
        txt_apellido = findViewById(R.id.txt_apellido);
        txt_correo = findViewById(R.id.txt_correo);
        txt_tel = findViewById(R.id.txt_tel);
        txt_ruc = findViewById(R.id.txt_ruc);
        txt_ci = findViewById(R.id.txt_ci);
        txt_person = findViewById(R.id.txt_person);
        txt_fecha_nacimiento = findViewById(R.id.txt_fecha_nacimiento);
        reg_button = findViewById(R.id.reg_button);

        reg_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               Register();
            }
        });
    }

    private void Register() {
        OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                .connectTimeout(90, TimeUnit.SECONDS)
                .readTimeout(90, TimeUnit.SECONDS)
                .writeTimeout(90, TimeUnit.SECONDS)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        RetrofitInterface  retrofitInterface = retrofit.create(RetrofitInterface.class);

//        String nombre = "Pedro";
//        String apellido = "Pascal";
//        String correo = "pascal_pedro@gmail.com";
//        String tel = "2432423";
//        String ruc = "453453";
//        String ci = "5.121.232";
//        String person = "FISICA";
//        String fecha_nacimiento =  "2000-12-02 00:00:00";

        String nombre = txt_nombre.getText().toString();
        String apellido = txt_apellido.getText().toString();
        String correo = txt_correo.getText().toString();
        String tel = txt_tel.getText().toString();
        String ruc = txt_ruc.getText().toString();
        String ci = txt_ci.getText().toString();
        String person = txt_person.getText().toString();
        String fecha_nacimiento = txt_fecha_nacimiento.getText().toString();

        Map<String, String> requestBody = new HashMap<>();
        requestBody.put("nombre", nombre);
        requestBody.put("apellido", apellido);
        requestBody.put("email", correo);
        requestBody.put("telefono", tel);
        requestBody.put("ruc", ruc);
        requestBody.put("cedula", ci);
        requestBody.put("tipoPersona", person);
        requestBody.put("fechaNacimiento", fecha_nacimiento);

        //        User usr = new User(nombre,apellido,correo,tel,ruc,ci,person,fecha_nacimiento);
        //        Log.d(TAG, "TEST: "+usr.getApellido());
        //        Call <User> call = retrofitInterface.uploadReg(usr);
        Call <Object> call = retrofitInterface.uploadReg(requestBody);
        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(registrar_person.this, "Usuario Registrado", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                Toast.makeText(registrar_person.this,"Error-Mensaje:"+t.getMessage(), Toast.LENGTH_LONG).show();
                Log.d(TAG, "onFailure: "+t.getMessage());
            }
        });


    }


}
