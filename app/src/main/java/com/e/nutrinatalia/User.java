package com.e.nutrinatalia;

public class User {
    String nombre;
    String apellido;
    String correo;
    String tel;
    String ruc;
    String ci;
    String person;
    String fecha_nacimiento;


    public User(String nombre, String apellido, String correo, String tel, String ruc, String ci, String person, String fecha_nacimiento){
        this.nombre = nombre;
        this.apellido = apellido;
        this.correo = correo;
        this.tel = tel;
        this.ruc = ruc;
        this.ci = ci;
        this.person = person;
        this.fecha_nacimiento =fecha_nacimiento;
    }



    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public String getCi() {
        return ci;
    }

    public void setCi(String ci) {
        this.ci = ci;
    }

    public String getPerson() {
        return person;
    }

    public void setPerson(String person) {
        this.person = person;
    }

    public String getFecha_nacimiento() {
        return fecha_nacimiento;
    }

    public void setFecha_nacimiento(String fecha_nacimiento) {
        this.fecha_nacimiento = fecha_nacimiento;
    }


}
